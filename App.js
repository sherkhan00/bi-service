import React from "react";
import Setup from "./src/boot/setup";
import checkToken from "./src/middlewares/checkToken";

class App extends React.Component {
  render() {
    return <Setup />;
  }
}

export default App;
