import React, { Component } from "react";
import { StyleProvider } from "native-base";
import App from "../App";
import getTheme from "../theme/components";
import variables from "../theme/variables/commonColor";
import Store from "./store";

export default class Setup extends Component {
  render() {
    return (
      <Store>
        <StyleProvider style={getTheme(variables)}>
          <App />
        </StyleProvider>
      </Store>
    );
  }
}
