import React, { Component } from "react";
import { createStore, combineReducers, applyMiddleware } from "redux";
import thunk from "redux-thunk";
import { Provider } from "react-redux";
import TokenReducer from "../reducers/token";
import UserReducer from "../reducers/user";

class Store extends Component {
  constructor(props) {
    super(props);

    this.store = createStore(combineReducers({
      token: TokenReducer,
      user: UserReducer
    }), applyMiddleware(thunk));
  }

  render() {
    return(
      <Provider store={this.store}>
        {this.props.children}
      </Provider>
    );
  }
}

export default Store;
