import React from "react";
import * as AsyncStorage from "react-native/Libraries/Storage/AsyncStorage";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import { setToken } from "../reducers/token/actions";

function checkToken(Component) {
  const mapStateToProps = ({user}) => ({
    user
  });

  const mapDispatchToProps = dispatch => bindActionCreators({
    setToken
  }, dispatch);

  return connect(mapStateToProps, mapDispatchToProps)(class extends React.Component {
    componentWillMount() {
      if (this.props.token === null) {
        AsyncStorage.getItem("token").then(token => {
          if (token !== null) {
            this.props.setToken(token);
          }
        });
      }
    }

    render() {
      return <Component />;
    }
  });
}

export default checkToken;
