import React, { Component } from "react";
import { Image } from "react-native";
import {
  Content,
  Text,
  List,
  ListItem,
  Icon,
  Container,
  Left,
  Right,
  Badge
} from "native-base";
import styles from "./style";
import { connect } from "react-redux";

const drawerCover = require("../../../assets/coverbi.png");

let datasWork = [];

const datas = [
  {
    name: "Заявки BI Service",
    route: "Requests",
    icon: "home",
    bg: "#C5F442"
  },
  {
    name: "Исполнители",
    route: "Performers",
    icon: "contacts",
    bg: "#C5F442"
  },
  {
    name: "Список ЖК",
    route: "RequestsOnRealEstates",
    icon: "ios-stats",
    bg: "#C5F442"
  },
  {
    name: "Уведомления жильцам",
    route: "NotificationsList",
    icon: "ios-notifications",
    bg: "#C5F442"
  },
  {
    name: "Мой Профиль",
    route: "Profile",
    icon: "person",
    bg: "#C5F442"
  },
  {
    name: "Выйти из учетной записи",
    route: "Login",
    icon: "power",
    bg: "#C5F442"
  }

];


const datasPerformerScreens = [
  {
    name: "Заявки BI Service",
    route: "Requests",
    icon: "home",
    bg: "#C5F442"
  },
  {
    name: "Мой Профиль",
    route: "Profile",
    icon: "person",
    bg: "#C5F442"
  },
  {
    name: "Выйти из учетной записи",
    route: "Login",
    icon: "power",
    bg: "#C5F442"
  }

];

class SideBar extends Component {
  state = {
    shadowOffsetWidth: 1,
    shadowRadius: 4,
    datasWork: []
  };

  renderSideBar = ({role}) => {
    if ([4, 5, 6].indexOf(role.id) > -1) {
      this.setState({datasWork: datasPerformerScreens});
    } else {
      this.setState({datasWork: datas});
    }
  };

  componentDidMount() {
    if (this.props.user !== null) {
      this.renderSideBar(this.props.user);
    }
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.user !== null) {
      this.renderSideBar(nextProps.user);
    }
  }

  render() {
    const {datasWork} = this.state;

    return (
      <Container>
        <Content
          bounces={false}
          style={{ flex: 1, backgroundColor: "#fff", top: -1 }}
        >
          <Image source={drawerCover} style={styles.drawerCover} />
          {/*<Image source={drawerImage} square style={styles.drawerImage}  />*/}
          {datasWork.length ? (
            <List
              dataArray={datasWork}
              renderRow={data =>
                <ListItem
                  button
                  noBorder
                  onPress={() => this.props.navigation.navigate(data.route)}
                >
                  <Left>
                    <Icon
                      active
                      name={data.icon}
                      style={{ color: "#777", fontSize: 26, width: 30 }}
                    />
                    <Text style={styles.text}>
                      {data.name}
                    </Text>
                  </Left>
                  {data.types &&
                  <Right style={{ flex: 1 }}>
                    <Badge
                      style={{
                        borderRadius: 3,
                        height: 25,
                        width: 72,
                        backgroundColor: data.bg
                      }}
                    >
                      <Text
                        style={styles.badgeText}
                      >{`${data.types} Types`}</Text>
                    </Badge>
                  </Right>}
                </ListItem>}
            />
          ) : null}
        </Content>
      </Container>
    );
  }
}

const mapStateToProps = ({user}) => ({
  user
});

const mapDispatchToProps = () => ({});

export default connect(mapStateToProps, mapDispatchToProps)(SideBar);
