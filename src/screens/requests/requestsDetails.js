import React, { Component } from "react";
import {
  Container,
  Header,
  Title,
  Content,
  Button as ReactNativeButton,
  Item,
  Input,
  Body,
  Left,
  Right,
  Icon,
  Form,
  Text,
  Label,
  Thumbnail, Textarea, Spinner, Tabs, ScrollableTab, Tab
} from "native-base";
import styles from "./styles";
import { MainApi } from "../api/MainApi";
import ImagePicker from "react-native-image-picker";
import Button from "../../components/Button";
import { withNavigation } from "react-navigation";

const defect = require("../../../assets/defect.jpg");

class RequestDetails extends Component {

  constructor(props) {
    super(props);
    this.state = {
      username: "",
      password: "",
      comment: "",
      avatarSource: null,
      photoProofBase64:null
    };
  }

  imagePicker = ImagePicker;
  imagePickerOptions = {
    title: "Выбрать...",
    storageOptions: {
      skipBackup: true,
      path: "images"
    },
    cancelButtonTitle: "Отмена",
    takePhotoButtonTitle: "Камера",
    chooseFromLibraryButtonTitle: "Галерея"
  };

  requestInProgress = () => {
    return new Promise((resolve, reject) => {
      let { requestItem } = this.props.navigation.state.params;


      MainApi.updateRequestGlobally(requestItem.Number, requestItem.IndexNumber, 2).then(() => {
        MainApi.updateRequestLocally(requestItem.Number, requestItem.IndexNumber, 2, requestItem.id, requestItem);
        MainApi.sendPushNotification(requestItem.ApplicationUserId,
          `Статус Вашей заявки № ${requestItem.Number}  изменился на статус "В работе"`, `Статус Вашей заявки № ${requestItem.Number} изменился на статус "В работе"`);
        resolve();
        alert("Статус успешно изменен");
        this.props.navigation.navigate("Requests");
      }).catch(() => {
        MainApi.updateRequestLocally(requestItem.Number, requestItem.IndexNumber, 2, requestItem.id, requestItem);
        MainApi.sendPushNotification(requestItem.ApplicationUserId,
          `Статус Вашей заявки № ${requestItem.Number}  изменился на статус "В работе"`, `Статус Вашей заявки № ${requestItem.Number} изменился на статус "В работе"`);
        reject();
        alert("Статус успешно изменен");
        this.props.navigation.navigate("Requests");
      });
    });
  };

  requestPerformered = () => {
    return new Promise((resolve, reject) => {
      let { requestItem } = this.props.navigation.state.params;

      MainApi.updateRequestGlobally(requestItem.Number, requestItem.IndexNumber, 3, this.state.comment, this.state.photoProofBase64)
        .then(() => {
          MainApi.updateRequestLocally(requestItem.Number, requestItem.IndexNumber, 3, requestItem.id, requestItem);
          resolve();
          alert("Статус успешно изменен");
          this.props.navigation.navigate("Requests");
        }).catch(() => {
          MainApi.updateRequestLocally(requestItem.Number, requestItem.IndexNumber, 3, requestItem.id, requestItem);
          reject();
          alert("Статус успешно изменен");
        this.props.navigation.navigate("Requests");
      });
    });
  };

  showImagePicker = () => {
    this.imagePicker.showImagePicker(this.imagePickerOptions, (response) => {

      if (response.didCancel) {
        console.log("User cancelled image picker");
      } else if (response.error) {
        console.log("ImagePicker Error: ", response.error);
      } else if (response.customButton) {
        console.log("User tapped custom button: ", response.customButton);
      } else {
        const source = { uri: response.uri };

        // const sourceBase64 = { uri: "data:image/jpeg;base64," + response.data };
        const sourceBase64 =  response.data;

        this.setState
        ({
          avatarSource: source,
          photoProofBase64: sourceBase64
        });
      }
    });
  };

  render() {
    let { requestItem } = this.props.navigation.state.params;
    return (
      <Container style={styles.container}>
        <Header>
          <Left>
            <ReactNativeButton transparent onPress={() => this.props.navigation.goBack()}>
              <Icon name="arrow-back"/>
            </ReactNativeButton>
          </Left>
          <Body>
          <Title>Детали заявки</Title>
          </Body>
          <Right/>
        </Header>
        <Content>
          <Form>
            <Item stackedLabel>
              <Label>Номер заявки</Label>
              <Input disabled value={requestItem.Number}/>
            </Item>
            <Item stackedLabel>
              <Label>Дата создания</Label>
              <Input disabled value={requestItem.DateTime}/>
            </Item>
            <Item stackedLabel>
              <Label>Описание</Label>
              <Text style={{ width: "100%", margin: 15 }}>{requestItem.Message}</Text>
            </Item>
            <Item stackedLabel>
              <Label>Контактное лцио</Label>
              <Text style={{ width: "100%", margin: 15 }}>{requestItem.ContactFullname}</Text>
            </Item>
            <Item stackedLabel>
              <Label>Контактный номер</Label>
              <Text style={{ width: "100%", margin: 15 }}>{requestItem.ContactPhone}</Text>
            </Item>
            <Item stackedLabel>
              <Label>Владелец</Label>
              <Text style={{ width: "100%", margin: 15 }}>{requestItem.ApplicationUserName}</Text>
            </Item>
            <Item stackedLabel>
              <Label>Желаемая дата закрытия</Label>
              <Text style={{
                width: "100%",
                margin: 15
              }}>{requestItem.WantDate.substr(0, requestItem.WantDate.indexOf("T"))}</Text>
            </Item>
            <Item stackedLabel>
              <Label>Желаемое время закрытия</Label>
              <Text style={{
                width: "100%",
                margin: 15
              }}> {requestItem.WantTime === "0" ? "до обеда" : "после обеда"}</Text>
            </Item>
            {/*<Textarea rowSpan={5} style={{ margin: 15, marginTop: 20 }} bordered disabled*/}
            {/*value={requestItem.Message} />*/}
            {/*<Item stackedLabel>*/}
            {/*<Label>Исполнитель</Label>*/}
            {/*<Input disabled value="Самат Алтай"/>*/}
            {/*</Item>*/}
          </Form>

          <Thumbnail square style={{  width: "100%", height: 200 }} source={defect}/>
          {([2, 5].indexOf(requestItem.StateId) > -1) ?
            <Item stackedLabel>
              <Label style={{ width: "100%", margin: 15 }}>Комментарий исполнителя</Label>
              <Input style={{ width: "100%" }} placeholder="" onChangeText={(comment) => this.setState({ comment })}/>
            </Item> : null}


          {
            requestItem.StateId === 1 ?
            <Button block warning style={{ margin: 15, marginTop: 50 }} onPress={this.requestInProgress} onPendingText="Сохранение данных...">
              Взять в работу
            </Button> : null
          }


          {
            ([2, 5].indexOf(requestItem.StateId) > -1) ?
              <ReactNativeButton bordered style={{ margin: 15, marginTop: 50 }} onPress={this.showImagePicker}>
                <Text>Прикрепить фото подтверждения</Text>
              </ReactNativeButton>
              : null
          }

          <Thumbnail style={{ width: "100%", height: 200 }} square large source={this.state.avatarSource}/>

          {
            ([2, 5].indexOf(requestItem.StateId) > -1) ?
              <Button block success style={{ margin: 15, marginTop: 50 }} onPress={this.requestPerformered} onPendingText="Сохранение данных...">
                Выполнено
              </Button> : null
          }

        </Content>
      </Container>
    );
  }
}

export default withNavigation(RequestDetails);
