import React, { Component } from "react";
import {
  Container,
  Header,
  Title,
  Content,
  Button,
  Item,
  Input,
  Body,
  Left,
  Right,
  Icon,
  Form,
  Text, Textarea, Label
} from "native-base";
import styles from "./styles";
import * as AsyncStorage from "react-native/Libraries/Storage/AsyncStorage";

class RequestsSearch extends Component {

  constructor(props) {
    super(props);
    this.state = {
      username: '',
      password:''
    };
  }
  handleClick = () => {
    fetch("http://178.128.52.206:3000/api/Users/login",
      { body: JSON.stringify(
          {
            username:`${this.state.username}`,
            password:`${this.state.password}`
          }
        ),
        headers: { Accept: "application/json", "Content-Type": "application/json" },
        method: "POST",
      }).then((resp) => resp.json())
      .then(
        function(data)
        {
          if(data.id){

            AsyncStorage.setItem('userId', data.id);
            AsyncStorage.getItem("userId").then((value) => {
            });
          }
          else
          {
            alert("Неверный логин или пароль!");
          }
        })

  }

  render() {
    return (
      <Container style={styles.container}>

        <Header>
          <Left>
            <Button transparent onPress={() => this.props.navigation.goBack()}>
              <Icon name="arrow-back" />
            </Button>
          </Left>
          <Body>
          <Title>Поиск заявок</Title>
          </Body>
          <Right />
        </Header>

        <Content>
          <Form>
            <Item stackedLabel>
              <Label>Номер заявки</Label>
              <Input/>
            </Item>
            <Item stackedLabel>
              <Label>Жилой комлекс</Label>
              <Input/>
            </Item>
            <Item stackedLabel>
              <Label>Статус</Label>
              <Input/>
            </Item>
          </Form>
          <Button block style={{ margin: 15, marginTop: 50 }} onPress={() => {this.handleClick()}}>
            <Text>Найти</Text>
          </Button>

        </Content>
      </Container>
    );
  }
}

export default RequestsSearch;
