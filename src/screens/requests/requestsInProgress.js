import React, { Component } from "react";
import { Content, Text, Body, ListItem, Left, Thumbnail, Badge, Button, List } from "native-base";
import { withNavigation } from "react-navigation";

const defect = require("../../../assets/defect.jpg");
const defect2 = require("../../../assets/defect2.jpg");

const birequests =
  [
    {
      "onec_id": "123456",
      "real_estate_id": 1,
      "creator_id": 1,
      "description": " Есть замечания по фасадной плитке объекта. Прошу исправить дефект на северной стороне здания, ближе к 1 подъезду",
      "created_date": "26 Сент",
      "id": 1,
      "img": defect,
      "status": "В работе"
    },
    {
      "onec_id": "123777",
      "real_estate_id": 1,
      "creator_id": 1,
      "description": "Капает труба в ванной.",
      "created_date": "27 Сент",
      "id": 2,
      "img": defect2,
      "status": "Перенесено"
    },
    {
      "onec_id": "123456",
      "real_estate_id": 1,
      "creator_id": 1,
      "description": "Есть замечания по фасадной плитке объекта. Прошу исправить дефект на северной стороне здания, ближе к 1 подъезду",
      "created_date": "26 Сент",
      "id": 1,
      "img": defect,
      "status": "В работе"
    },
    {
      "onec_id": "123777",
      "real_estate_id": 1,
      "creator_id": 1,
      "description": "Капает труба в ванной.",
      "created_date": "27 Сент",
      "id": 2,
      "img": defect2,
      "status": "Просрочено"
    },
    {
      "onec_id": "123777",
      "real_estate_id": 1,
      "creator_id": 1,
      "description": "Капает труба в ванной.",
      "created_date": "27 Сент",
      "id": 2,
      "img": defect2,
      "status": "В работе"
    }
  ];


class RequestsInProgress extends Component {

  constructor(props) {
    super(props);
    this.handleClick = this.handleClick.bind(this);
  }

  handleClick = (birequest) => {
    this.props.navigation.push("RequestDetails", { requestItem: birequest });
  };


  render() {
    let {requests} = this.props;
    return (
      <Content padder>
        <List
          dataArray={requests}
          renderRow={birequest =>
            <ListItem thumbnail button onPress={() => {
              this.handleClick(birequest);
            }}>
              <Left>
                <Thumbnail square source={defect}/>
              </Left>
              <Body>
              <Text>
                №:{birequest.Number}-{birequest.IndexNumber} от {birequest.DateTime}
              </Text>
              <Text numberOfLines={2} note>
                {birequest.Message}
              </Text>
              <Badge danger={birequest.StateName === "Просрочено" ? true : false}
                     warning={birequest.StateName === "Просрочено" ? false : true}>
                <Text style={{ fontSize: 12 }}>{birequest.StateName}</Text>
              </Badge>
              </Body>
            </ListItem>}
        />

      </Content>
    );
  }
}

export default withNavigation(RequestsInProgress);
