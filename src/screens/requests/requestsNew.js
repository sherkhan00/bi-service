import React, { Component } from "react";
import { Content, Text, Body, ListItem, Left, Thumbnail, Badge, Button, List } from "native-base";
import { withNavigation } from "react-navigation";

const defect = require("../../../assets/defect.jpg");

class RequestsNew extends Component {

  constructor(props) {
    super(props);
    this.handleClick = this.handleClick.bind(this);
  }

  handleClick = (birequest) => {
    this.props.navigation.push("RequestDetails", { requestItem: birequest });
  };

  render() {
    let { requests } = this.props;

    console.log(requests);


    return (
      <Content padder>
        <List
          dataArray={requests}
          renderRow={birequest =>
            <ListItem thumbnail button onPress={() => {
              this.handleClick(birequest);
            }}>
              <Left>
                <Thumbnail square source={defect}/>
              </Left>
              <Body>
              <Text>
                №:{birequest.Number}-{birequest.IndexNumber} от {birequest.DateTime}
              </Text>
              <Text numberOfLines={2} note>
                {birequest.Message}
              </Text>
              <Badge success>
                <Text style={{ fontSize: 12 }}>{birequest.StateName}</Text>
              </Badge>
              </Body>
            </ListItem>}
        />

      </Content>
    );
  }
}

export default withNavigation(RequestsNew);
