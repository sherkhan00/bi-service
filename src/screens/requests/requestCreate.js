import React, { Component } from "react";
import {
  Container,
  Header,
  Title,
  Content,
  Button,
  Item,
  Input,
  Body,
  Left,
  Right,
  Icon,
  Form,
  Text, Textarea, Label,
  Thumbnail, Picker
} from "native-base";
import styles from "./styles";
import { MainApi } from "../api/MainApi";
import ImagePicker from "react-native-image-picker";


class CreateRequest extends Component {
  state = {
    message: "",
    avatarSource: null,
    selected1: "key1"
  };
  imagePicker = ImagePicker;
  imagePickerOptions = {
    title: "Выбрать...",
    storageOptions: {
      skipBackup: true,
      path: "images"
    },
    cancelButtonTitle: "Отмена",
    takePhotoButtonTitle: "Камера",
    chooseFromLibraryButtonTitle: "Галерея"
  };
  handleClick = () => {

    MainApi.postNewBiServiceInternalRequest(this.state.message).then((response) => {
      if (response.Message) {
        this.props.navigation.goBack();
      }
    });
  };

  onValueChange(value: string) {
    this.setState({
      selected1: value
    });
  }

  showImagePicker = () => {
    this.imagePicker.showImagePicker(this.imagePickerOptions, (response) => {

      if (response.didCancel) {
        console.log("User cancelled image picker");
      } else if (response.error) {
        console.log("ImagePicker Error: ", response.error);
      } else if (response.customButton) {
        console.log("User tapped custom button: ", response.customButton);
      } else {
        const source = { uri: response.uri };

        // You can also display the image using data:
        // const source = { uri: 'data:image/jpeg;base64,' + response.data };

        this.setState({
          avatarSource: source
        });
      }
    });
  };


  render() {
    return (
      <Container style={styles.container}>

        <Header>
          <Left>
            <Button transparent onPress={() => this.props.navigation.goBack()}>
              <Icon name="arrow-back"/>
            </Button>
          </Left>
          <Body>
          <Title>Создать заявку</Title>
          </Body>
          <Right/>
        </Header>

        <Content>
          <Form>
            <Item stackedLabel>
              <Label>Объект</Label>
              <Input disabled value="Bi City Seoul"/>
            </Item>
            <Textarea rowSpan={5} style={{ margin: 15, marginTop: 20 }} bordered
                      placeholder="Введите описание замечания" onChangeText={(message) => this.setState({ message })}/>

            <Item>
              <Label>Исполнитель</Label>
              <Picker
                note
                mode="dropdown"
                style={{ width: 120 }}
                selectedValue={this.state.selected1}
                onValueChange={this.onValueChange.bind(this)}
              >
                <Item label="Сантехник" value="key0" />
                <Item label="Плотник" value="key1" />
                <Item label="Электрик" value="key1" />
              </Picker>
            </Item>
          </Form>
          <Thumbnail style={{ margin: 15 }} square large source={this.state.avatarSource}/>

          <Button block style={{ margin: 15, marginTop: 50 }} onPress={this.showImagePicker}>
            <Text>Прикрепить фото</Text>
          </Button>
          <Button block style={{ margin: 15 }} onPress={() => {
            this.handleClick();
          }}>
            <Text>Создать</Text>
          </Button>
        </Content>
      </Container>
    );
  }
}

export default CreateRequest;
