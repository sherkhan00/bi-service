import React, { Component } from "react";
import {
  Container,
  Header,
  Title,
  Button,
  Icon,
  Left,
  Right,
  Body,
  Tabs,
  ScrollableTab,
  Tab, Spinner
} from "native-base";

import RequestsNew from "./requestsNew";
import RequestsInProgress from "./requestsInProgress";
import RequestsNonActive from "./requestsNonActive";
import * as AsyncStorage from "react-native/Libraries/Storage/AsyncStorage";
import { MainApi } from "../api/MainApi";
import { connect } from "react-redux";


class Requests extends Component {
  constructor(props) {
    super(props);
    this.state = {
      active: false,
      reqsNew: [],
      reqsInProgress: [],
      reqsNonActive: [],
      loaded: false
    };
  }

  componentDidMount() {
    this.getRequests();
  }

  getRequests = () => {
    let token = "";
    let userId = "";

    AsyncStorage.getItem("token").then((value) => {
      token = value;

      AsyncStorage.getItem("userId").then((value) => {
        userId = value;

        MainApi.getRequestsByManagerProfileId(userId, token).then((response) => {
          if (!response.error) {
            let reqsNew = [];
            let reqsInProgress = [];
            let reqsNonActive = [];
            let manager_real_estates = response.manager_real_estate;
            manager_real_estates.map(item => {
              let requests = item.real_estate.onec_requests;

              if (typeof requests !== "undefined" || requests.length) {
                requests.map(innerItem => {
                  switch (innerItem.StateId) {
                    case 1:
                      reqsNew.push(innerItem);
                      break;
                    case 2:
                      reqsInProgress.push(innerItem);
                      break;
                    case 3:
                      reqsNonActive.push(innerItem);
                      break;
                    case 4:
                      reqsNonActive.push(innerItem);
                      break;
                    case 5:
                      reqsInProgress.push(innerItem);
                      break;
                    case 6:
                      reqsNonActive.push(innerItem);
                      break;
                    case 7:
                      reqsNonActive.push(innerItem);
                      break;
                    case 8:
                      reqsNonActive.push(innerItem);
                      break;
                    default:
                      reqsNew.push(innerItem);
                  }
                });
              }
            });
            this.setState({ reqsNew: reqsNew, reqsInProgress: reqsInProgress, reqsNonActive: reqsNonActive, loaded: true });
          } else {
            alert("Что-то пошло не так!");
          }
        });
      });
    });
  };

  compare = (a, b) => {
    if (a.id > b.id)
      return -1;
    if (a.id < b.id)
      return 1;
    return 0;
  };

  /*componentWillReceiveProps(nextProps) {
    const {params: oldParams} = this.props.navigation.state;
    const {params} = nextProps.navigation.state;

    if (typeof params !== "undefined" && typeof params.updatedItem !== "undefined") {
      if (typeof oldParams.updatedItem !== "undefined" && oldParams.updatedItem.id !== params.updatedItem.id) {
        this.updateRequests(params.updatedItem.type, params.updatedItem.item);
      } else if (typeof oldParams.updatedItem === "undefined") {
        this.updateRequests(params.updatedItem.type, params.updatedItem.item);
      }
    }
  }*/

  updateRequests = (type, request) => {
    let {reqsNew, reqsInProgress, reqsNonActive} = this.state;

    switch (type) {
      case "nonActive":
        reqsInProgress = reqsInProgress.filter(item => item.id !== request.id);

        reqsNonActive.unshift({...request, StateId: 3});

        return this.setState({reqsInProgress, reqsNonActive});
      case "inProgress":
        reqsNew = reqsNew.filter(item => item.id !== request.id);

        reqsInProgress.unshift({...request, StateId: 2});

        return this.setState({reqsNew, reqsInProgress});
      default:
        return false;
    }
  };

  render() {
    let { loaded, reqsNew, reqsInProgress, reqsNonActive } = this.state;

    return (
      <Container>
        <Header hasTabs>
          <Left>
            <Button
              transparent
              onPress={() => this.props.navigation.navigate("DrawerOpen")}
            >
              <Icon name="menu"/>
            </Button>
          </Left>
          <Body>
          <Title>Заявки BI Service</Title>
          </Body>
          <Right>
            {/*<Button transparent*/}
                    {/*onPress=*/}
              {/*{*/}
                {/*() =>*/}
                  {/*this.props.navigation.push("RequestsSearch")*/}
              {/*}*/}
            {/*>*/}
              {/*<Icon name="search"/>*/}
            {/*</Button>*/}
            <Button transparent onPress={() => this.props.navigation.push("CreateRequest")}>
              <Icon name="add"/>
            </Button>
          </Right>
        </Header>
        {!loaded ? <Spinner color="blue"/> :
          <Tabs renderTabBar={() => <ScrollableTab style={{ backgroundColor: "#05509b" }}/>}>
            <Tab heading="Новые">
              <RequestsNew updateRequests={this.updateRequests} requests={reqsNew.sort(this.compare)}/>
            </Tab>
            <Tab heading="В процессе">
              <RequestsInProgress updateRequests={this.updateRequests} requests={reqsInProgress.sort(this.compare)}/>
            </Tab>
            <Tab heading="Неактивные">
              <RequestsNonActive updateRequests={this.updateRequests} requests={reqsNonActive.sort(this.compare)}/>
            </Tab>
          </Tabs>
        }
      </Container>
    );
  }
}

const mapStateToProps = ({user}) => ({
  user
});

const mapDispatchToProps = () => ({});

export default connect(mapStateToProps, mapDispatchToProps)(Requests);
