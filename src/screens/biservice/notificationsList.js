import React, { Component } from "react";
import {
  Container,
  Header,
  Title,
  Content,
  Button,
  Item,
  Input,
  Body,
  Left,
  Right,
  Icon,
  Form,
  Text, Label, ListItem, Thumbnail, List, Spinner
} from "native-base";
import styles from "./styles";
import * as AsyncStorage from "react-native/Libraries/Storage/AsyncStorage";
import { MainApi } from "../api/MainApi";

class NotificationsList extends Component {

  state = {
    notifications: []
  };

  componentDidMount() {
    this.getNotifications();
  }

  getNotifications = () => {
    let token = "";
    let userId = "";

    AsyncStorage.getItem("token").then((value) => {
      token = value;

      AsyncStorage.getItem("userId").then((value) => {
        userId = value;

        MainApi.getNotificationsByManagerUserId(userId, token).then((response) => {
          if (!response.error) {
            let notifications = [];
            let manager_real_estates = response.manager_real_estate;
            manager_real_estates.map(item => {
              let notifies = item.real_estate.notifications;
              if (notifies != undefined || notifies.length != 0) {
                notifies.map(innerItem => {
                  notifications.push(innerItem);
                });
              }
            });
            this.setState({ notifications: notifications, loaded: true });
          } else {
            alert("Что-то пошло не так!");
          }
        });
      });
    });
  };

  render() {
    const { notifications, loaded } = this.state;
    return (
      <Container style={styles.container}>

        <Header>
          <Left>
            <Button transparent onPress={() => this.props.navigation.navigate("DrawerOpen")}>
              <Icon name="menu"/>
            </Button>
          </Left>
          <Body style={{ flex: 3 }}>
          <Title>Уведомления жильцам</Title>
          </Body>
          <Right/>
        </Header>
        {!loaded ? <Spinner color="blue"/> :
          <Content>
            <List
              dataArray={notifications}
              renderRow={data =>
                <ListItem thumbnail>
                  <Body>
                  <Text>
                    {data.message}
                  </Text>
                  <Text numberOfLines={1} note>
                    {data.type}
                  </Text>
                  </Body>
                </ListItem>
              }
            />
            <Button block style={{ margin: 15, marginTop: 50 }} onPress={() => {
              this.props.navigation.push("SendNotification");
            }}>
              <Text>Создать уведомление</Text>
            </Button>
          </Content>
        }
      </Container>
    );
  }
}

export default NotificationsList;
