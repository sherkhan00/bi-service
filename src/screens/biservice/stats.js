import React, { Component } from "react";
import {
  Container,
  Header,
  Title,
  Content,
  Button,
  Item,
  Input,
  Body,
  Left,
  Right,
  Icon,
  Form,
  Text, Label, ListItem, Thumbnail, List, Spinner
} from "native-base";
import styles from "./styles";
import * as AsyncStorage from "react-native/Libraries/Storage/AsyncStorage";
import { MainApi } from "../api/MainApi";

const datas = [
  {
    text: "Времена года - Лето",
    note: "",
    time: "3:43 pm"
  },
  {
    text: "Арнау 1",
    note: "",
    time: "1:12 pm"
  },
  {
    text: "Dream city 1",
    note: "",
    time: "10:03 am"
  },
  {
    text: "Времена года - Осень",
    note: "",
    time: "1:12 pm"
  },
  {
    text: "Зеленый квартал B2",
    note: "",
    time: "10:03 am"
  },
  {
    text: "Dream city 1",
    note: "",
    time: "1:12 pm"
  },
  {
    text: "Зеленый квартал B4",
    note: "",
    time: "10:03 am"
  }
];

class RequestsOnRealEstates extends Component {

  componentDidMount()
  {
    this.getUserInfo();
  }

  state = {
      real_estates:[],
    loaded:false
  };

  getUserInfo = () => {
    let token = "";
    let userId = "";

    AsyncStorage.getItem("token").then((value) => {
      token = value;

      AsyncStorage.getItem("userId").then((value) => {
        userId = value;

        MainApi.getUserInfoOptimized(userId, token).then((response) => {

          if (!response.error)
          {
            this.setState({ real_estates: response.manager_real_estate, loaded:true});

          } else
          {
            alert("Что-то пошло не так!");
          }
        });

      });
    });
  };
  render() {

    const { real_estates,loaded } = this.state;

    return (
      <Container style={styles.container}>

        <Header>
          <Left>
            <Button transparent onPress={() => this.props.navigation.navigate("DrawerOpen")}>
              <Icon name="menu" />
            </Button>
          </Left>
          <Body>
          <Title>Список ЖК</Title>
          </Body>
          <Right />
        </Header>
        {!loaded ? <Spinner color="blue"/> :
          <Content>
            <List
              dataArray={real_estates}
              renderRow={data =>
                <ListItem avatar>
                  <Body>
                  <Text>
                    {data.real_estate.name}
                  </Text>
                  <Text numberOfLines={1} note>
                    {data.real_estate.real_name}
                  </Text>
                  </Body>
                </ListItem>}
            />
          </Content>
        }
      </Container>
    );
  }
}

export default RequestsOnRealEstates;
