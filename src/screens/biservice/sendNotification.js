import React, { Component } from "react";
import {
  Container,
  Header,
  Title,
  Content,
  Button as ButtonReactNative,
  Item,
  Input,
  Body,
  Left,
  Right,
  Icon,
  Form,
  Text,
  Textarea,
  Label
} from "native-base";
import styles from "./styles";
import { MainApi } from "../api/MainApi";
import Button from "../../components/Button";

class SendNotification extends Component {
  state = {
    message: '',
    title: ''
  };

  handleClick = () => {
    return new Promise((resolve, reject) => {

      MainApi.sendNotification(this.state.message, this.state.title).then(response => {
        alert("Отправлено клиентам");
        resolve();
      }).catch(error => reject());
    });
  };

  render() {
    return (
      <Container style={styles.container}>

        <Header>
          <Left>
            <ButtonReactNative transparent onPress={() => this.props.navigation.goBack()}>
              <Icon name="arrow-back" />
            </ButtonReactNative>
          </Left>
          <Body style={{flex: 3}}>
          <Title>Отправить уведомление</Title>
          </Body>
          <Right />
        </Header>

        <Content>
          <Form>
            <Item stackedLabel>
            <Label>Объект</Label>
            <Input disabled value="BI-City Seoul"/>
          </Item>
            <Item stackedLabel>
              <Label>Тема</Label>
              <Input placeholder="" onChangeText={(title) => this.setState({ title })}/>

            </Item>
            <Textarea rowSpan={5} style={{ margin: 15, marginTop: 20 }} bordered placeholder="Введите текст уведомления" onChangeText={(message) => this.setState({ message })}  />
          </Form>

          <Button block style={{ margin: 15, marginTop: 50 }} onPress={this.handleClick} onPendingText="Отправка уведомления">
            Отправить уведомление
          </Button>

        </Content>
      </Container>
    );
  }
}

export default SendNotification;
