import React, { Component } from "react";
import {
  Container,
  Header,
  Title,
  Content,
  Button,
  Item,
  Input,
  Body,
  Left,
  Right,
  Icon,
  Form,
  Text, Label
} from "native-base";
import styles from "./styles";
import * as AsyncStorage from "react-native/Libraries/Storage/AsyncStorage";

class PerformerDetails extends Component {

  constructor(props) {
    super(props);
    this.state = {
      username: '',
      password:''
    };
  }
  handleClick = () => {
    fetch("http://178.128.52.206:3000/api/Users/login",
      { body: JSON.stringify(
          {
            username:`${this.state.username}`,
            password:`${this.state.password}`
          }
        ),
        headers: { Accept: "application/json", "Content-Type": "application/json" },
        method: "POST",
      }).then((resp) => resp.json())
      .then(
        function(data)
        {
          if(data.id){

            AsyncStorage.setItem('userId', data.id);
            AsyncStorage.getItem("userId").then((value) => {
              //alert(value);
            });
          }
          else
          {
            alert("Неверный логин или пароль!");
          }
        })

  }

  render() {
    return (
      <Container style={styles.container}>
        <Header>
          <Left>
            <Button transparent onPress={() => this.props.navigation.goBack()}>
              <Icon name="arrow-back" />
            </Button>
          </Left>
          <Body style={{flex: 3}}>
          <Title>Детали исполнителя</Title>
          </Body>
          <Right />
        </Header>

        <Content>
          <Form>
            <Item stackedLabel>
              <Label>Логин</Label>
              <Input disabled value="77770552446"/>
            </Item>
            <Item stackedLabel>
              <Label>ФИО</Label>
              <Input disabled value="Асхатов Расул"/>
            </Item>
            <Item stackedLabel>
              <Label>Деятельность</Label>
              <Input disabled value="Сантехник"/>
            </Item>
            <Item stackedLabel>
              <Label>Дата регистрации</Label>
              <Input disabled value="25/05/2018"/>
            </Item>
          </Form>
          <Button block style={{ margin: 15, marginTop: 50 }} onPress={() => this.props.navigation.push("PerformerResetPassword")}>
            <Text>Сбросить пароль</Text>
          </Button>


        </Content>
      </Container>
    );
  }
}

export default PerformerDetails;
