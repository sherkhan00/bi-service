import React, { Component } from "react";
import {
  Container,
  Header,
  Title,
  Content,
  Button,
  Item,
  Input,
  Body,
  Left,
  Right,
  Icon,
  Form,
  Text, Label, Picker
} from "native-base";
import styles from "./styles";
import * as AsyncStorage from "react-native/Libraries/Storage/AsyncStorage";

class NewPerformer extends Component {

  constructor(props) {
    super(props);
    this.state = {
      selectedItem: undefined,
      selected1: "key1",
      results: {
        items: []
      }
    };
  }
  onValueChange(value: string) {
    this.setState({
      selected1: value
    });
  }


  handleClick = () => {
    fetch("http://178.128.52.206:3000/api/Users/login",
      { body: JSON.stringify(
          {
            username:`${this.state.username}`,
            password:`${this.state.password}`
          }
        ),
        headers: { Accept: "application/json", "Content-Type": "application/json" },
        method: "POST",
      }).then((resp) => resp.json())
      .then(
        function(data)
        {
          if(data.id){

            AsyncStorage.setItem('userId', data.id);
            AsyncStorage.getItem("userId").then((value) => {
              alert(value);
            });
          }
          else
          {
            alert("Неверный логин или пароль!");
          }
        })

  }

  render() {
    return (
      <Container style={styles.container}>
        <Header>
          <Left>
            <Button transparent onPress={() => this.props.navigation.goBack()}>
              <Icon name="arrow-back" />
            </Button>
          </Left>
          <Body style={{flex: 3}}>

          <Title>Новый исполнитель</Title>
          </Body>
          <Right />
        </Header>

        <Content>
          <Form>
            <Item stackedLabel>
              <Label>Мобильный телефон</Label>
              <Input/>
            </Item>
            <Item stackedLabel>
              <Label>ФИО</Label>
              <Input/>
            </Item>
            <Item>
              <Label>Деятельность</Label>
              <Picker
                note
                mode="dropdown"
                style={{ width: 120 }}
                selectedValue={this.state.selected1}
                onValueChange={this.onValueChange.bind(this)}
              >
                <Item label="Сантехник" value="key0" />
                <Item label="Плотник" value="key1" />
                <Item label="Электрик" value="key1" />
              </Picker>
            </Item>
          </Form>
          <Button block style={{ margin: 15, marginTop: 50 }} onPress={() => {this.handleClick()}}>
            <Text>Добавить</Text>
          </Button>

        </Content>
      </Container>
    );
  }
}

export default NewPerformer;
