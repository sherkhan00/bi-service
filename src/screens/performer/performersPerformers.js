import React, { Component } from "react";
import {
  Container,
  Header,
  Title,
  Content,
  Button,
  Body,
  Left,
  Right,
  Icon,
  Text, Label, ListItem, Thumbnail, List, Spinner
} from "native-base";
import styles from "./styles";
import * as AsyncStorage from "react-native/Libraries/Storage/AsyncStorage";
import { MainApi } from "../api/MainApi";

const datas = [
  {
    text: "Аманжолов Еркин",
    note: "Электрик",
    time: "3:43 pm"
  },
  {
    text: "Асхатов Расул",
    note: "Электрик",
    time: "1:12 pm"
  },
  {
    text: "Даникеров Бекзат",
    note: "Сантехник",
    time: "10:03 am"
  },
  {
    text: "Асхатов Расул",
    note: "Сантехник",
    time: "1:12 pm"
  },
  {
    text: "Даникеров Бекзат",
    note: "Плотник",
    time: "10:03 am"
  },
  {
    text: "Асхатов Расул",
    note: "Плотник",
    time: "1:12 pm"
  },
  {
    text: "Даникеров Бекзат",
    note: "Плотник",
    time: "10:03 am"
  }
];

class Performers extends Component {

  state = {
    loaded: false,
    username: "",
    password: "",
    masters: [],
  };

  getPerformersList = () => {
    let token = "";
    let userId = "";

    AsyncStorage.getItem("token").then((value) => {
      token = value;

      AsyncStorage.getItem("userId").then((value) => {
        userId = value;

        MainApi.getMastersByManagerIdOptimized(userId, token).then((response) => {
          if (!response.error) {

            this.setState({ masters: response.manager, loaded:true});

          } else {
          }
        });
      });
    });
  };

  getMastersInfo = (items, token, index = 0, masters = [], recursive, callback) => {
    if (recursive) {
      MainApi.getUserInfo(items[index].master_id, token).then((response) => {
        if (!response.error) {
          masters.push(response);
        }

        this.getMastersInfo(items, token, index + 1, masters, index < items.length - 1, callback);
      });
    } else {
      if (typeof callback !== "undefined") {
        callback(masters);
      }
    }
  };

  render() {
    const { masters, loaded } = this.state;
    return (
      <Container style={styles.container}>

        <Header>
          <Left>
            <Button transparent onPress={() => this.props.navigation.navigate("DrawerOpen")}>
              <Icon name="menu"/>
            </Button>
          </Left>
          <Body>
          <Title>Исполнители</Title>
          </Body>
          <Right/>
        </Header>
        {!loaded ? <Spinner color="blue"/> :
          <Content>
            <List
              dataArray={masters}
              renderRow={list =>
                <ListItem thumbnail button onPress={() => {
                  //this.props.navigation.push("PerformerDetails");
                }}>
                  <Body>
                  <Text>
                    {list.master.fio}
                  </Text>
                  <Text numberOfLines={1} note>
                    {list.master.positions.name}
                  </Text>
                  </Body>
                </ListItem>}
            />
            <Button block style={{ margin: 15, marginTop: 50 }} onPress={() => {
              this.props.navigation.push("NewPerformer");
            }}>
              <Text>Добавить исполнителя</Text>
            </Button>


          </Content>
        }
      </Container>
    );
  }

  componentDidMount() {
    this.getPerformersList();
  }
}

export default Performers;

