import React, { Component } from "react";
import {
  Container,
  Header,
  Title,
  Content,
  Button,
  Item,
  Input,
  Body,
  Left,
  Right,
  Icon,
  Form,
  Text, Label
} from "native-base";
import styles from "./styles";
import * as AsyncStorage from "react-native/Libraries/Storage/AsyncStorage";
import { MainApi } from "../api/MainApi";

class ChangePassword extends Component {

  constructor(props) {
    super(props);
    this.state = {
      currentPassword: "",
      newPassword: "",
      confirmPassword: ""
    };
  }

  handleClick = () => {
    const { currentPassword, newPassword, confirmPassword } = this.state;

    if (newPassword === confirmPassword && currentPassword != newPassword) {
      AsyncStorage.getItem("token").then((token) => {
        MainApi.userChangePassword(currentPassword, newPassword, token).then((response) => {
          if (response.ok) {

            alert("Успешно! Пароль изменен!");

          } else {
            alert(`Неверный пароль!`);
          }
        }).catch(error => {
        });
      });

    } else {
      alert("Пароли не совпадают (либо указан тот старый пароль)");
    }


  };

  render() {
    return (
      <Container style={styles.container}>
        <Header>
          <Left>
            <Button transparent onPress={() => this.props.navigation.goBack()}>
              <Icon name="arrow-back"/>
            </Button>
          </Left>
          <Body>
          <Title>Сменить пароль</Title>
          </Body>
          <Right/>
        </Header>

        <Content>
          <Form>
            <Item stackedLabel>
              <Label>Текущий пароль</Label>
              <Input secureTextEntry
                     onChangeText={(currentPassword) => this.setState({ currentPassword })}/>
            </Item>
            <Item stackedLabel>
              <Label>Новый пароль</Label>
              <Input secureTextEntry
                     onChangeText={(newPassword) => this.setState({ newPassword })}/>
            </Item>
            <Item stackedLabel>
              <Label>Подтверждение</Label>
              <Input secureTextEntry
                     onChangeText={(confirmPassword) => this.setState({ confirmPassword })}/>
            </Item>
          </Form>
          <Button block style={{ margin: 15, marginTop: 50 }} onPress={() => {
            this.handleClick();
          }}>
            <Text>Сменить пароль</Text>
          </Button>

        </Content>
      </Container>
    );
  }
}

export default ChangePassword;
