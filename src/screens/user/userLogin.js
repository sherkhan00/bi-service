import React, { Component } from "react";

import {
  Container,
  Header,
  Title,
  Content,
  Button,
  Item,
  Input,
  Body,
  Right,
  Form,
  Text
} from "native-base";
import styles from "./styles";
import * as AsyncStorage from "react-native/Libraries/Storage/AsyncStorage";
import { MainApi } from "../api/MainApi";
import connect from "react-redux/es/connect/connect";
import { setToken } from "../../reducers/token/actions";
import { bindActionCreators } from "redux";
import { setUser } from "../../reducers/user/actions";

class Login extends Component {
  state = {
    username: "",
    password: ""
  };

  setUser = (token, userId) => {
    const { navigate } = this.props.navigation;

    this.props.setToken(token);
    this.getRoleIdByUserId(userId, token);

    AsyncStorage.setItem("token",token);
    AsyncStorage.setItem("userId",userId.toString());

    navigate("Requests");
  };

  handleClick = () => {
    //const { username, password } = this.state;
    const username = "87756711566";
    const password = "1996";

    MainApi.userLogin(username, password).then(({ id: token, userId }) => {
      if (token !== null) {
        this.setUser(token, userId);
      } else {
        alert("Неверный логин или пароль!");
      }
    }).catch(error => {
      alert("Телефон не подключен к сети");
    });
  };

  masterLoginDemo = () => {
    //const { username, password } = this.state;
    //Карамурзаев Марат (Сантехник)
    const username = "87024072758";
    const password = "1995";

    MainApi.userLogin(username, password).then(({ id: token, userId }) => {
      if (token !== null) {
        this.setUser(token, userId);
      } else
        {
        alert("Неверный логин или пароль!");
      }
    }).catch(error => {
      alert("Телефон не подключен к сети");
    });
  };

  getRoleIdByUserId = (id, token) => {
    MainApi.getRoleIdByUserIdFetch(id, token).then(({role_id}) => {
      this.props.setUser({
        id: id,
        role: {
          id: role_id,
          name: null
        }
      });
    }).catch(() => {
      alert("Профиль не найден");
    });
  };

  render() {
    return (
      <Container style={styles.container}>
        <Header>
          <Body>
          <Title>Bi Service</Title>
          </Body>
          <Right/>
        </Header>

        <Content>
          <Form>
            <Item>
              <Input placeholder="логин" onChangeText={(username) => this.setState({ username })}/>
            </Item>
            <Item last>
              <Input placeholder="пароль" secureTextEntry onChangeText={(password) => this.setState({ password })}/>
            </Item>
          </Form>
          <Button block style={{ margin: 15, marginTop: 50 }} onPress={() => {
            this.handleClick();
          }}>
            <Text>Демо вход</Text>
          </Button>
          {/*<Button block style={{ margin: 15 }} onPress={() => {*/}
            {/*this.masterLoginDemo();*/}
          {/*}}>*/}
            {/*<Text>Войти как Мастер</Text>*/}
          {/*</Button>*/}
        </Content>
      </Container>
    );
  }
}

const mapStateToProps = ({user}) => ({
  user
});

const mapDispatchToProps = dispatch => bindActionCreators({
  setToken,
  setUser
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Login);
