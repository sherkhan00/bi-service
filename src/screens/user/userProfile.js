import React, { Component } from "react";
import {
  Container,
  Header,
  Title,
  Content,
  Button,
  Item,
  Input,
  Body,
  Left,
  Right,
  Icon,
  Spinner,
  Form,
  Text, Label
} from "native-base";
import styles from "./styles";
import { MainApi } from "../api/MainApi";
import { setUser } from "../../reducers/user/actions";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";


class Profile extends Component {

  componentDidMount() {
    this.getUserInfo();
  }

  constructor(props) {
    super(props);
    this.state = {
      user:
        {
          fio: "",
          user_id: 0,
          phone: "",
          role_id: 0,
          id: 0,
          positions: {
            name: "",
            id: 0
          },
          manager_real_estate: [
          ]
        },
      token: "",
      userId: "",
      username: "",
      password: "",
      loaded: false,
      position: "",
      realEstateName: ""
    };
  }

  getUserInfo = () => {
    const {token, user} = this.props;

    MainApi.getUserInfoOptimized(user.id, token).then((response) => {
      if (!response.error) {
        this.setState({ user: response, loaded:true});

      } else {
        alert("Что-то пошло не так!");
      }
    });
  };

  render() {
    const { user, position, loaded, realEstateName } = this.state;

    return (
      <Container style={styles.container}>
        <Header>
          <Left>
            <Button transparent onPress={() => this.props.navigation.navigate("DrawerOpen")}>
              <Icon name="menu"/>
            </Button>
          </Left>
          <Body>
          <Title>Мой профиль</Title>
          </Body>
          <Right/>
        </Header>
        {!loaded ? <Spinner color="blue"/> :
          <Content>
            <Form>
              <Item stackedLabel>
                <Label>ФИО</Label>
                <Input disabled value={user.fio}/>
              </Item>
              <Item stackedLabel>
                <Label>Роль</Label>
                <Input disabled value={user.positions.name}/>
              </Item>
              <Item stackedLabel>
                <Label>Жилые комплексы</Label>
                  <Text style={{width: "100%", margin: 15}}>{user.manager_real_estate.map(value => value.real_estate.name).join('\n')}</Text>
              </Item>
              <Item stackedLabel>
                <Label>Логин</Label>
                <Input disabled value={user.phone}/>
              </Item>
            </Form>

            <Button block style={{ margin: 15, marginTop: 50 }} onPress={() => {
              this.props.navigation.push("ChangePassword");
            }}>
              <Text>Сменить пароль</Text>
            </Button>
          </Content>
        }

      </Container>
    );
  }
}

const mapStateToProps = ({token, user}) => ({
  token,
  user
});

const mapDispatchToProps = dispatch => bindActionCreators({
  setUser
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Profile);
