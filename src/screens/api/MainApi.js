const API_URL = "http://178.128.52.206:3000/api/";

export class MainApi {

  static getDate(date) {
    var event = new Date(date);
    let mothA = "января,февраля,марта,апреля,мая,июня,июля,августа,сентября,октября,ноября,декабря".split(",");
    return event.getDay() + " " + mothA[event.getMonth()] + " " + event.getFullYear();
  }

  static unifetch = (URL) => {
    return fetch(API_URL + URL).then(response => response.json())
      .then((responseJson) => {
        return responseJson;
      });
  };

  static unifetchPOST = (URL, body) => {
    return fetch(API_URL + URL,
      {
        body: JSON.stringify(body),
        headers: { Accept: "application/json", "Content-Type": "application/json" },
        method: "POST"
      }).then(response => response.json()).then((responseJson) => {
      return responseJson;
    });
  };

  static unifetchPUT = (URL, body) => {
    return fetch(API_URL + URL,
      {
        body: JSON.stringify(body),
        headers: { Accept: "application/json", "Content-Type": "application/json" },
        method: "PUT"
      }).then(response => response.json()).then((responseJson) => {
      return responseJson;
    });
  };

  static unifetchPOSTFormData = (URL, data) => {
    var formBody = [];
    for (var property in data) {
      var encodedKey = encodeURIComponent(property);
      var encodedValue = encodeURIComponent(data[property]);
      formBody.push(encodedKey + "=" + encodedValue);
    }
    formBody = formBody.join("&");

    return fetch(API_URL + URL,
      {
        headers: {
          "Content-Type": "application/x-www-form-urlencoded;charset=UTF-8"
        },
        body: formBody,
        method: "POST"
      }).then(response => {
        return response;
      }
    );
  };


  static userLogin(username, password) {
    try {
      let body = {
        username: username,
        password: password
      };
      return this.unifetchPOST("Users/login", body).then((responseJson) => {
        return responseJson;
      });
    } catch (error) {
      console.log("Error when call API (userLogin): " + error.message);
    }
  }

  static getRoleIdByUserIdFetch(id, token) {
    try {
      let filter = `profiles/findOne?filter={"where":{"user_id":${id}}}&access_token=${token}`;
      return this.unifetch(filter).then((responseJson) => {
        return responseJson;
      });
    } catch (error) {
      console.log("Error when call API (): " + error.message);
    }
  }


  static getUserInfo(userId, token) {
    try {
      let filter = `profiles/findOne?filter={"where":{"user_id":${userId}}}&access_token=${token}`;

      return this.unifetch(filter).then((responseJson) => {
        //alert(JSON.stringify(responseJson));
        return responseJson;
      });
    } catch (error) {
      console.log("Error when call API (): " + error.message);
    }
  }


  static getUserInfoOptimized(userId, token) {
    try {
      let filter = `profiles/findOne?filter={ "where":{"user_id":${userId}},  "include": [{"relation":  "positions" }, {"relation": "manager_real_estate","scope": {"include": ["real_estate"]} }] }&access_token=${token}`;

      return this.unifetch(filter).then((responseJson) => {
        return responseJson;
      });
    } catch (error) {
      console.log("Error when call API (): " + error.message);
    }
  }

  static getMastersByManagerIdOptimized(userId, token) {
    try {
      let filter = `profiles/findOne?filter={"where":{"user_id":${userId}},  "include": {"relation": "manager",  "scope": {"include": {"relation": "master",  "scope": {"include": ["positions"]       }}       }}}&access_token=${token}`;
      return this.unifetch(filter).then((responseJson) => {
        return responseJson;
      });
    } catch (error) {
      console.log("Error when call API (): " + error.message);
    }
  }

  static getNotificationsByManagerUserId(userId, token) {
    try {
      // let filter = `profiles/findOne?filter={"where":{"user_id":${userId}},"include": {"relation": "manager",  "scope": {"include": ["master"]}}}&access_token=${token}`;
      let filter = `profiles/findOne?filter={"where":{"user_id":${userId}},"include": {"relation": "manager_real_estate",  "scope": {"include": {"relation": "real_estate",  "scope": {"include": ["notifications"]       }       } }} }&access_token=${token}`;
      return this.unifetch(filter).then((responseJson) => {
        return responseJson;
      });
    } catch (error) {
      console.log("Error when call API (): " + error.message);
    }
  }

  static userChangePassword(old, newPassword, token) {
    try {
      let body = {
        oldPassword: old,
        newPassword: newPassword
      };
      return this.unifetchPOSTFormData(`Users/change-password?access_token=${token}`, body).then((responseJson) => {
        return responseJson;
      });
    } catch (error) {
      console.log("Error when call API (userChangePassword): " + error.message);
    }
  }

  static getRequestsByManagerProfileId(userId, token) {
    try {
      let filter = `profiles/findOne?filter={"where":{"user_id":${userId}},"include": {"relation": "manager_real_estate",  "scope": {"include": {"relation": "real_estate",  "scope": {"include": ["onec_requests"]       }       } }} }&access_token=${token}`;
      return this.unifetch(filter).then((responseJson) => {
        return responseJson;
      });
    } catch (error) {
      console.log("Error when call API (): " + error.message);
    }
  }


  static postNewBiServiceInternalRequest(message) {
    try {
      let body =
        {
          Number: "Внутренняя заявка",
          DateTime: new Date().toISOString(),
          ApplicationUserId: "66fdd059-652d-4d72-8c94-d0ad1c9f8733",
          realEstateGuid: "0d9efdec-c8cf-11e3-882b-0025906b4dd5",
          performerGuid: "66fdd059-652d-4d72-8c94-d0ad1c9f8733",
          ApplicationUserName: "Токарев Валерий Геннадьевич",
          ContactFullname: "Иван",
          ContactPhone: "+77778885566",
          PhotoProof: "NULL",
          IndexNumber: 1,
          Message: message,
          ReqPhoto: "NULL",
          StateId: 1,
          StateName: "Внутренняя",
          Mark: 0,
          Comment: "NULL",
          WantDate: "NULL",
          WantTime: "NULL",
          RemarkType: "66fdd059-652d-4d72-8c94-d0ad1c9f8733",
          DateOfClose: "NULL",
          DateOfMaster: "NULL"
        };
      return this.unifetchPOST("onec_requests", body).then((responseJson) => {
        return responseJson;
      });
    } catch (error) {
      console.log("Error when call API (userLogin): " + error.message);
    }
  }

  static sendNotification(message, type) {
    return fetch("http://webapi.bi-group.org/api/v1/MobileClient/PostNotificationToBuilding",
      {
        body: JSON.stringify({
          RealEstateGuid: "f1709d5e-97ea-11e4-bd2b-0025906b4dd5ss",
          Message: message,
          Type: type,
          Source: 51
        }),
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
          token: "749389e0-2af7-4810-adbc-c89bb32924b7"
        },
        method: "POST",
        credentials: "include"

      }).then(response => {
      if (response.ok) {
        return response.json();
      } else {
        return {
          status: response.status,
          message: response.statusText
        };
      }
    });
  }

  static updateRequestGlobally(docNumber, index, statusId, comment, photoProofBase64) {
    return fetch("http://webapi.bi-group.org/api/v1/MobileClient/PostRatingBiService",
      {
        body: JSON.stringify({
          Remark: index,
          DocumentNumber: docNumber,
          statusid: statusId,
          Comment: comment,
          PhotoProof: photoProofBase64
        }),
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
          token: "749389e0-2af7-4810-adbc-c89bb32924b7"
        },
        method: "POST",
        credentials: "include"

      }).then(response => {
      if (response.ok) {
        return response.json();
      } else {
        return {
          status: response.status,
          message: response.statusText
        };
      }
    });
  }

  static updateRequestLocally(Number, reqIndex, StatusId, id, item) {
    let StateName = "Новая";

    switch (StatusId) {
      case 1:
        StateName = "Новая";
        break;
      case 2:
        StateName = "В работе";
        break;
      case 3:
        StateName = "Выполнено";
        break;
      case 4:
        StateName = "Отклонено";
        break;
      case 5:
        StateName = "Перенесено";
        break;
      case 6:
        StateName = "Закрыта";
        break;
      case 7:
        StateName = "Задолженность";
        break;
      case 8:
        StateName = "Отклонено клиентом";
        break;
    }

    try {
      let body =
        {
          Number: Number,
          IndexNumber: reqIndex,
          StateId: StatusId,
          id: id,
          DateTime: item.DateTime,
          ApplicationUserId: item.ApplicationUserId,
          realEstateGuid: item.realEstateGuid,
          performerGuid: item.performerGuid,
          ApplicationUserName: item.ApplicationUserName,
          ContactFullname: item.ContactFullname,
          ContactPhone: item.ContactPhone,
          PhotoProof: item.PhotoProof,
          Message: item.Message,
          ReqPhoto: item.ReqPhoto,
          StateName: StateName,
          Mark: item.Mark,
          Comment: item.Comment,
          WantDate: item.WantDate,
          WantTime: item.WantTime,
          RemarkType: item.RemarkType,
          DateOfClose: item.DateOfClose,
          DateOfMaster: item.DateOfMaster
        };
      return this.unifetchPUT("onec_requests", body).then((responseJson) => {
        return responseJson;
      });
    } catch (error) {
      console.log("Error when call API (userLogin): " + error.message);
    }
  }


  static sendPushNotification(ApplicationUserId, Message, Type) {
    return fetch("http://webapi.bi-group.org/api/v1/MobileClient/PostNotificationByApplicationUserId",
      {
        body: JSON.stringify(
          {
            ApplicationUserId: ApplicationUserId,
            Message: Message,
            Type: Type,
            Source: 51
          }),
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
          token: "749389e0-2af7-4810-adbc-c89bb32924b7"
        },
        method: "POST",
        credentials: "include"

      }).then(response => {
      if (response.ok) {
        return response.json();
      } else {
        return {
          status: response.status,
          message: response.statusText
        };
      }
    });
  }
}
