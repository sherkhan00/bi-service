import React from "react";
import { Button as NativeBaseButton, Text } from "native-base";

class Button extends React.Component {
  state = {
    pending: false
  };
  onPress = () => {
    this.setState({pending: true});
    this.props.onPress().then(() => {
      this.setState({pending: false});
    }).catch(() => {
      this.setState({pending: false});
    });
  };
  render() {
    const {onPress, disabled, onPendingText, ...rest} = this.props;
    const {pending} = this.state;

    return (
      <NativeBaseButton {...rest} disabled={pending} onPress={this.onPress}>
        <Text>{pending ? onPendingText : this.props.children}</Text>
      </NativeBaseButton>
    );
  }
}

export default Button;
