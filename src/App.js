import React from "react";
import { Root } from "native-base";
import { StackNavigator, DrawerNavigator } from "react-navigation";

import Profile from "./screens/user/userProfile";
import Login from "./screens/user/userLogin";
import ChangePassword from "./screens/user/userChangepassword";
import Logout from "./screens/user/userLogout";

import Home from "./screens/home/";
import SideBar from "./screens/sidebar";
import RequestsOnRealEstates from "./screens/biservice/stats";
import SendNotification from "./screens/biservice/sendNotification";

import Performers from "./screens/performer/performersPerformers";
import NewPerformer from "./screens/performer/performerAdd";
import PerformerDetails from "./screens/performer/performerDetails";
import PerformerResetPassword from "./screens/performer/performerResetPassword";

import CreateRequest from "./screens/requests/requestCreate";
import Requests from "./screens/requests/requestsRequests";
import RequestsSearch from "./screens/requests/requestsSearch";
import RequestsInProgress from "./screens/requests/requestsInProgress";
import RequestsNonActive from "./screens/requests/requestsNonActive";
import RequestsNew from "./screens/requests/requestsNew";
import RequestDetails from "./screens/requests/requestsDetails";
import NotificationsList from "./screens/biservice/notificationsList";
import checkToken from "./middlewares/checkToken";


const Drawer = DrawerNavigator(
  {
    Home: { screen: Home },

    Requests: { screen: Requests },
    Login: { screen: Login,
      navigationOptions: {
        drawerLockMode: 'locked-closed'
      }},
    RequestsNew: { screen: RequestsNew },
    RequestsInProgress : { screen: RequestsInProgress  },
    RequestsNonActive : { screen: RequestsNonActive  },

    RequestDetails: { screen: RequestDetails },
    Profile: { screen: Profile },
    CreateRequest: { screen: CreateRequest },
    ChangePassword: { screen: ChangePassword },
    Logout: { screen: Logout },
    Performers: { screen: Performers },
    NewPerformer: { screen: NewPerformer },
    SendNotification: { screen: SendNotification },
    RequestsSearch: { screen: RequestsSearch },
    PerformerDetails: { screen: PerformerDetails },
    RequestsOnRealEstates: { screen: RequestsOnRealEstates },
    PerformerResetPassword: { screen: PerformerResetPassword },
    NotificationsList: { screen: NotificationsList },

  },
  {
    initialRouteName: "Login",
    contentOptions: {
      activeTintColor: "#e91e63"
    },
    contentComponent: props => <SideBar {...props} />
  }
);

const AppNavigator = StackNavigator(
  {
    Drawer: { screen: Drawer },
    Login: { screen: Login },
    RequestsNew: { screen: RequestsNew },
    RequestsInProgress: { screen: RequestsInProgress },
    RequestsNonActive: { screen: RequestsNonActive },
    RequestDetails: { screen: RequestDetails },
    Profile: { screen: Profile },
    CreateRequest: { screen: CreateRequest },
    ChangePassword: { screen: ChangePassword },
    Logout: { screen: Logout },
    Performers: { screen: Performers },
    NewPerformer: { screen: NewPerformer },
    RequestsOnRealEstates: { screen: RequestsOnRealEstates },
    SendNotification: { screen: SendNotification },
    RequestsSearch: { screen: RequestsSearch },
    PerformerDetails: { screen: PerformerDetails },
    PerformerResetPassword: { screen: PerformerResetPassword },
    NotificationsList: { screen: NotificationsList },
  },
  {
    initialRouteName: "Drawer",
    headerMode: "none"
  }
);

export default checkToken(
  () => (
    <Root>
      <AppNavigator />
    </Root>
  )
);
