import { SET_TOKEN } from "./actions";
import * as AsyncStorage from "react-native/Libraries/Storage/AsyncStorage";

const initialState = null;

function TokenReducer(state = initialState, action) {
  switch (action.type) {
    case SET_TOKEN:
      AsyncStorage.setItem("token", action.payload.toString());

      return action.payload;
    default:
      return state;
  }
}

export default TokenReducer;
