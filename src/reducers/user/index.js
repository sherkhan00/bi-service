import { SET_USER } from "./actions";
import * as AsyncStorage from "react-native/Libraries/Storage/AsyncStorage";

const initialState = null;

function UserReducer(state = initialState, action) {
  switch (action.type) {
    case SET_USER:
      AsyncStorage.setItem("user", JSON.stringify({...action.payload}));

      return action.payload;
    default:
      return state;
  }
}

export default UserReducer;
